/**
*@file
*File to handel event on page.
*/
(function ($) {
  'use strict';

  /**
   * Load remote content after the main page loaded.
   */
  Drupal.behaviors.full_page_search_form = {
    attach: function (context, settings) {
      // Define elements.
      var searchbox = '#full-page-search input[type="text"]';
      var search_popup_class = '#full-page-search';
      var search_form = '#full-page-search form';
      var search_wraper = '#full-page-search-form';
      var search_close_button = '#full-page-search button.close';
      var search_results_wraper = '#full-search-form-result';
      $(search_wraper + ', #search-form input[type="text"], #search-block-form input[type="text"]').click(function (e) {
        e.preventDefault();
        $(search_popup_class).removeClass('hide');
        $(search_popup_class).addClass('open');
        $(searchbox).css('border', '2px solid #000');
        $(search_form).css(top, '30%');
        $(searchbox).focus();
      });
      window.addEventListener('keydown', function (e) {
        if (e.keyCode === 114 || (e.ctrlKey && e.keyCode === 70)) {
          e.preventDefault();
          $(search_popup_class).addClass('open');
          $(search_popup_class).removeClass('hide');
          $(searchbox).css('border', '2px solid #000');
          $(searchbox).focus();
        }
      });
      $(search_popup_class + ',' + search_close_button).click(function (e) {
        if (e.target.className === 'close') {
          $(this).removeClass('open');
          $(search_popup_class).addClass('hide');
          $(searchbox).val('');
          $(search_results_wraper).html('');
        }
      });
      $(document, context).keyup(function (e) {
        if (e.keyCode === 27) {
          $(this).removeClass('open');
          $(search_popup_class).addClass('hide');
          $(searchbox).val('');
          $(search_results_wraper).html('');
        }
      });
      $(searchbox).keypress(function () {
        $(search_form).animate({top: '10%'}, 1000);
        $('#full-page-search #full-search-form-result-box').animate({top: '23%', height: '70%'}, 1000);
      });
      $('#full-search-form-result-box ul.pager li a').click(function (e) {
        e.preventDefault();
        var valid_url = this.href;
        var current_page_array = valid_url.split('?');
        var pager_url = Drupal.settings.full_page_search_form.pager_url;
        var keys_value = Drupal.settings.full_page_search_form.search_key;
        var thisUrl = pager_url + keys_value + '?' + current_page_array[1];
        $.ajax({
          url: thisUrl,
          type: 'GET',
          success: function (data) {
            $('#full-search-form-result').html(data);
            Drupal.attachBehaviors('#full-search-form-result');
          }
        });
        return false;
      });

    }
  };
})(jQuery);
