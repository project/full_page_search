<?php

/**
 * @file
 * Theme implementation for displaying search results.
 */
?>
<li class="search-result-colmn"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <h3>
    <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </h3>
  <?php print render($title_suffix); ?>
  <div class="search-snippet-info ">
    <?php if ($snippet): ?>
      <p class="search-desc"<?php print $content_attributes; ?>><?php print $snippet; ?></p>
    <?php endif; ?>
    <?php if ($info): ?>
      <p class="search-info content-created-box"><?php print $info; ?></p>
    <?php endif; ?>
  </div>
</li>
