Introduction:
-------------------
This module provide functionality to open search from on full page 
and you can also open search form by click of CTRL+F.

If you want to add this functionality on any link.
Please add "full-page-search-form" id with element tag.
as: <a href="#" id="full-page-search-form">Click here</a>

Requirements:
------------------
This module requires the following modules:
* Jquery Colorpicker
* Search

Installation:
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

How To Use:
-----------------
1. Go to Structure -> Full page search settings (Configure design).
2. Click Ctrl+f or click on search box.
